//= partials/full-width-slider.js
//= partials/full-width-horizontal-slider.js
//= partials/gallery-slider.js
//= partials/reviews-slider.js
//= partials/room-numbers.js

$('#slider-main-form').submit(function() {
    var phone = $('#slider-form-phone').val();
    var fio = $('#slider-form-fio').val();

    $.ajax({
        url: config.hotelroom.request,
        method: 'POST',
        data: {
            phone: phone,
            fio: fio
        },
        dataType: 'json',
        success: function(response) {
            if (typeof response.success !== 'undefined') {
                location.href = config.hotelroom.successUrl;
            } else if (typeof response.error !== 'undefined') {
                if (typeof response.error.fio !== 'undefined') {
                    $('#slider-fio-error').html(response.error.fio);
                }
                if (typeof response.error.phone !== 'undefined') {
                    $('#slider-phone-error').html(response.error.phone);
                }
            }
        },
        error: function(errors) {
            $('#booking-common-error').html('Произошла непридвиненная ошибка. Приносим извинения');
            return false;
        }
    });
    return false;
});
$('.index-contacts-content-btn').click(function () {
    $('#bookingModal').modal('toggle');
    return false;
});

function onViewport(el, elClass, offset, callback) {
    /*** Based on http://ejohn.org/blog/learning-from-twitter/ ***/
    var didScroll = false;
    var this_top;
    var height;
    var top;

    if(!offset) { var offset = 0; }

    $(window).scroll(function() {
        didScroll = true;
    });

    setInterval(function() {
        if (didScroll) {
            didScroll = false;
            top = $(this).scrollTop();

            $(el).each(function(i){
                this_top = $(this).offset().top - offset;
                height   = $(this).height();

                // Scrolled within current section
                if (top >= this_top && !$(this).hasClass(elClass)) {
                    $(this).addClass(elClass);

                    if (typeof callback == "function") callback(el);
                }
            });
        }
    }, 100);
}

$(document).ready(function () {

});