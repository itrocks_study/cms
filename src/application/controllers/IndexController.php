<?php

class IndexController extends Zend_Controller_Action {

    public function mainMenuAction() {
        $action = $this->_request->getParam('action');
        $alias = $this->_request->getParam('alias');
        $module = $this->_request->getParam('module');
        $controller = $this->_request->getParam('module');

        $modules = Bootstrap::getModuleList();
        $this->view->assign('modules', $modules);

        $this->view->assign('action', $action);
        $this->view->assign('controller', $controller);
        $this->view->assign('module', $module);
        $this->view->assign('alias', $alias);
    }

    public function headerAction() {
        $settingsModel = new Model_Settings();
        $data = $settingsModel->getSettings(['address', 'phone', 'email']);
        $this->view->assign('data', $data);
    }

    public function socialIconsAction() {
        $settingsModel = new Model_Settings();
        $social     = $settingsModel->getSettings(['vk','facebook', 'twitter', 'odnoklassniki', 'instagram']);
        $this->view->assign('social', $social);
    }

    public function contactsFooterAction() {
        $settingsModel = new Model_Settings();
        $contacts     = $settingsModel->getSettings(['address', 'phone', 'email']);

        $this->view->assign('contacts', $contacts);
    }

    public function contactsBlockAction() {
        $settingsModel = new Model_Settings();
        $contacts     = $settingsModel->getSettings(['address', 'phone', 'email']);

        $this->view->assign('contacts', $contacts);
    }

    public function headerNavTopAction(){
        $settingsModel = new Model_Settings();
        $contacts     = $settingsModel->getSettings(['phone']);

        $this->view->assign('contacts', $contacts);
    }
   
    public function searchAction() {
        $modules = Bootstrap::getModuleList();
        $searchModel = new Model_Search();
        $request = trim($this->getParam('query'));
        $buttonTitle = "buttonSearchTitle";
        $items = [];
        $errorString = "";

        if ($this->_request->isGet()) {
                $buttonTitle = "buttonSearchTryAgain";
                if (strlen($request) > 0) {

                    $items = $searchModel->search($request);
                    if (empty($items)) {
                        $errorString = "resultsNotFound";
                    }
                } else {
                    $errorString = "minimumCharInStringSearch";
                }
        }

        $this->view->assign('modules', $modules);
        $this->view->assign('searchQuery', $request);
        $this->view->assign('searchTitle', $buttonTitle);
        $this->view->assign('items', $items);
        $this->view->assign('errorString', $errorString);
    }

    public function feedbackAction(){
//        $diyTable = new Static_Model_DbTable_Feedback();
//        $diy = $diyTable->createRow();
//        $form = new Static_Form_Feedback($diy);
//
//        if ($this->_request->isPost()) {
//            $formData = $this->_request->getPost();
//            if ($form->isValid($formData)) {
//                try {
//                    $formData['date'] = date('Y-m-d H:i:s');
//                    $diy->setFromArray($formData);
//                    $diy->save();
//                        $mailModel = new Model_Mail();
//                        $mailModel->sendRequestFeedbackFormToAdmin($formData, $this->view->baseUrl());
//                    $this->_helper->redirector('feedback-success', 'index', 'default');
//                } catch (Exception $e) {
//                    $this->view->assign('error', $e->getMessage());
//                }
//            } else {
//
//            }
//        }
//        $this->view->assign('form', $form);
    }

    public function feedbackSuccessAction(){

    }

    public function footerAction() {
        $settingsModel = new Model_Settings();
        $data = $settingsModel->getSettings(['yandexMetrika', 'googleAnalytics', 'address', 'phone', 'email']);

        $this->view->assign('data', $data);
    }

    public function sliderAction(){
        $sliderTable = new Static_Model_DbTable_Slider();
        $images = $sliderTable->getSliderImages();
        $this->view->assign('images', $images);
    }

    public function headerLogoAction() {
        $settingsModel = new Model_Settings();
        $logo     = $settingsModel->getSettings(['siteLogo']);

        $this->view->assign('logo', $logo['siteLogo']);
    }

    public function footerLogoAction() {
        $settingsModel = new Model_Settings();
        $logo     = $settingsModel->getSettings(['siteLogo']);

        $this->view->assign('logo', $logo['siteLogo']);
    }
}